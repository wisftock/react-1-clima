import React, { Fragment, useEffect, useState } from 'react';
import Clima from './components/Clima';
import Error from './components/Error';
import Formulario from './components/Formulario';
import Header from './components/Header';

function App() {
  const initialState = {
    ciudad: '',
    pais: '',
  };
  const [search, setSearch] = useState(initialState);
  const [consult, setConsult] = useState(false);
  const [resultado, setResultado] = useState({});
  const [error, setError] = useState(false);
  const { ciudad, pais } = search;

  useEffect(() => {
    const consultarApi = async () => {
      const appId = 'db491c370b80e19e656b0a3fde4b46d8';
      if (consult) {
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        setResultado(resultado);
        setConsult(false);

        // no encontrado
        if (resultado.cod === '404') {
          setError(true);
        } else {
          setError(false);
        }
      }
    };
    consultarApi();
  }, [consult, ciudad, pais]);
  // console.log(error);
  let componente;
  if (error) {
    componente = <Error mensaje='Ciudad no encontrada' />;
  } else {
    componente = <Clima resultado={resultado} />;
  }
  return (
    <Fragment>
      <Header title={'Aplicación de clima'} />
      <div className='contenedor-form'>
        <div className='container'>
          <div className='row'>
            <div className='col m6 12'>
              <Formulario
                search={search}
                setSearch={setSearch}
                setConsult={setConsult}
              />
            </div>
            <div className='col m6 12'>{componente}</div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
